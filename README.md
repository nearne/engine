# NEARNE
The engine is mainly a personal project to research different programming
structures. Mainly the combination of being cross-platform (Windows, Ubuntu,
FreeBSD, Android, WASM, MacOS and iOS), and without the use of the conventional
use of libraries; only using sys-calls and hardware specific interfaces that are
dynamically linked such as as OpenGL, Vulkan, OSS, PulseAudio etc.

 - [Documentation](./code/README.md)
 - [Credits](./CREDITS.md)

## Licensing
Each file is under the license of the LICENSE.md file in the directory of the
file - or if none present in the folder, the first parent folder containing a
LICENSE.md file. If no license can be applied, due to no LICENSE.md file present
in the directory or in the parent directories the work is under exclusive
copyright by default.

## Folder structure
 - `/asset`: Files other than code such as sound, models and textures.
 - `/build`: Export directory
 - `/code`: Project source
 - `/doc`: Documentation and resources
 - `/solution`:  Platform specific files in order to compile

## Compiling
Inside `/code` are shell scripts for each build platform. For Unix systems
-FreeBSD, Ubuntu, etc.- use `/code/unix.sh`. For Windows systems, use
`/code/win.bat`. Append the `--help` argument to receive details on the usage of
the shell scripts.
