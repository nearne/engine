/**
 * Basic functionality:
 * - Detecting what compiler/platform/system currently running.
 * - Foundation types to be able to write cross-platform code.
 * - Extended foundation types such as float3 and mutex.
 * - Memory management and console/string formatting/printing
 */

#include "compiler/import.hpp"
#include "platform/import.hpp"
#include "standard/import.hpp"
#include "system/import.hpp"

#if NEARNE_DEBUG
	#include "test/import.hpp"
#endif
