/**
 * == COMPILER ==
 * The compiler section defines a set of directives used to preprocess the code.
 * Its goal is to give uniformity between different compilers such as Microsoft
 * Visual C++ compiler and compilers that use the LLVM backend.
 * 
 * TODO: I am not sure about some of the macro implementations (such as iterate,
 * iterate_r and foreach). They are a bit too syntactic sugary and provide less
 * benefit then should be. For now I'll leave them be, but use them with caution
 */

#define var auto

#if _MSC_VER
	#include "msvc.hpp"
#else
	#include "llvm.hpp"
#endif

#if NEARNE_EDITOR
	#define IF_EDITOR(x) x
#else
	#define IF_EDITOR(x)
#endif

#if NEARNE_DEBUG
	#define IF_DEBUG(x) x
#else
	#define IF_DEBUG(x)
#endif

#define CRASH *(volatile int*)0=0
#if NEARNE_DEBUG
	#define assert(expr) if (!(expr)) { *(volatile int*)0=0; }
	#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
	#define NotImplemented assert(!"NotImplemented")
#else
	#define assert(expr)
	#define s_assert(expr)
	#define NotImplemented NotImplemented!
#endif

#define ARRAY_LEN(a) (sizeof(a)/sizeof(a[0]))
#define CAST_ANY(x) reinterpret_cast<void*&>(x)

#define COMMA ,
#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b) 
#define MACRO_AS_STRING(x) MACRO_AS_STRING_(x)
#define MACRO_AS_STRING_(x) #x

#define FILE_LINE __FILE__ "::" MACRO_AS_STRING(__LINE__)
#define FILE_LINE_FMT " (" __FILE__ "::" MACRO_AS_STRING(__LINE__) ")"
#define VAR(name) CONCAT(name ## __NEARNE_MACRO_VAR_, __LINE__)

#define iterate(i, arr)   for (int i = 0, VAR(length) = len(arr); i < VAR(length); ++i)
#define iterate_r(i, arr) for (int i = len(arr) - 1; i >= 0; --i)
#define foreach(v, arr)   \
	for (int VAR(keep) = 1, VAR(i) = 0, VAR(length) = len(arr);\
         VAR(keep) && VAR(i) < VAR(length); VAR(keep) ^= 1, VAR(i)++)\
    		for (v = arr[VAR(i)]; VAR(keep); VAR(keep) ^= 1)


#define ALIGN_POW2(v, a) (((v)+((a)-1)) & ~(((v)-(v))+(a)-1))
#define ALIGN_8(v) (((v)+7)&~7)
#define ALIGN_16(v) (((v)+15)&~15)
#define IS_POW2(v) (((v) & ~((v) - 1)) == (v))

#define KILOBYTE (1<<10)
#define MEGABYTE (1<<20)
#define GIGABYTE (1<<30)

// When cstddef is not included, use a custom offsetof implementation
#ifndef offsetof
	#define offsetof(t, p) ((uptr)&(((t*)0)->p))
#endif
