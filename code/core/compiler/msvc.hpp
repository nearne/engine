/**
 * Go to https://learn.microsoft.com/en-us/cpp/preprocessor/predefined-macros
 * for a list of predefined MSVC macros.
 */

#define COMPILER_MSVC 1

#if _M_ARM || _M_ARM_ARMV7VE
	#define NEARNE32 1
	#define NEARNE_ARM32 1
	#define NEARNE_ARM 1
#elif _M_ARM64 || _M_ARM64EC
	#define NEARNE64 1
	#define NEARNE_ARM64 1
	#define NEARNE_ARM 1
#elif _M_IX86
	#define NEARNE32 1
	#define NEARNE_X32 1
	#define NEARNE_X86 1
#elif _M_AMD64 || _M_X64
	#define NEARNE64 1
	#define NEARNE_X64 1
	#define NEARNE_X86 1
#else
	#error Unknown architecture
#endif

#define READ_BARRIER _ReadBarrier()
#define WRITE_BARRIER _WriteBarrier()
#define ATOMIC_COMPARE_EXCHANGE(v, n, e) _InterlockedCompareExchange((long volatile *)v, n, e)
#define ATOMIC_EXCHANGE(v, n) _InterlockedExchange64((__int64 volatile *)v, n)
#define ATOMIC_ADD(v, a) _InterlockedExchangeAdd64((__int64 volatile *)v, a)
