#define COMPILER_LLVM 1

#if defined(__wasm64__)
	#define NEARNE64 1
	#define NEARNE_WASM64 1
	#define NEARNE_WASM 1
#elif defined(__wasm32__)
	#define NEARNE32 1
	#define NEARNE_WASM32 1
	#define NEARNE_WASM 1
#elif defined(__arm__)
	#define NEARNE32 1
	#define NEARNE_ARM32 1
	#define NEARNE_ARM 1
#elif defined(__aarch64__)
	#define NEARNE64 1
	#define NEARNE_ARM64 1
	#define NEARNE_ARM 1
#elif defined(__i386__)
	#define NEARNE32 1
	#define NEARNE_X32 1
	#define NEARNE_X86 1
#elif defined(__x86_64__) || defined(__ppc64__)
	#define NEARNE64 1
	#define NEARNE_X64 1
	#define NEARNE_X86 1
#else
	#error Unknown architecture
#endif

#define READ_BARRIER asm volatile("" ::: "memory")
#define WRITE_BARRIER asm volatile("" ::: "memory")
#define ATOMIC_COMPARE_EXCHANGE(v, n, e) __sync_val_compare_and_swap(v, e, n)
#define ATOMIC_EXCHANGE(v, n) __sync_lock_test_and_set(v, n)
#define ATOMIC_ADD(v, a) __sync_fetch_and_add(v, a)
