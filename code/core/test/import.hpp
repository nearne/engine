void test_core()
{
	assert(sizeof(uptr) == sizeof(void*));

	assert(sizeof(byte) == 1);
	assert(sizeof(sbyte) == 1);
	assert(sizeof(char) == 1);
	assert(sizeof(uchar) == 1);
	assert(sizeof(short) == 2);
	assert(sizeof(ushort) == 2);
	assert(sizeof(int) == 4);
	assert(sizeof(uint) == 4);
	assert(sizeof(s64) == 8);
	assert(sizeof(u64) == 8);
	
	assert(sizeof(float) == 4);
	assert(sizeof(double) == 8);
	
	assert(sizeof(float2) == 8);
	assert(sizeof(float3) == 12);
	assert(sizeof(float4) == 16);
	assert(sizeof(float2x2) == 16);
	assert(sizeof(float3x3) == 36);
	assert(sizeof(float4x4) == 64);
}
