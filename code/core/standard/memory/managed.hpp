/**
 * Creates a new pool with the settings from another
 */
memory::pool pool(memory::pool pool)
{
	memory::pool result {};
	result.allocator = pool.allocator;
	result.alignment = pool.alignment;
	return result;
}

/**
 * CAUTION: manually set the previous of the one next of current to null!
 */
inline void recycle_internal(memory::pool* pool, memory::managed* current)
{
	var allocator = pool->allocator;
	while (current)
	{
		var previous = current->previous;
		recycle(allocator, current);
		current = previous;
	}
}

/**
 * Places blocks deeper then _depth_ in the recycling bin ready to be reused
 */
inline void recycle(struct memory::pool* pool, int depth)
{
	var previous = pool->current;
	var current = previous;
	while (current && depth > 0)
	{
		previous = current;
		current = current->previous;
		depth--;
	}
	
	if (current)
	{
		recycle_internal(pool, current);
		previous->previous = null;
	}
}

/**
 * Places all blocks in the recycling bin ready to be reused
 */
inline void recycle(struct memory::pool* pool)
{
	recycle_internal(pool, pool->current);
	pool->current = null;
}

/**
 * Preparing an append operation - making sure the memory block has enough room
 * to put the desired data on.
 * TODO: Split for a forced new allocation
 */
inline uptr prepare(memory::pool* pool, uptr initial_size)
{
	/**
	 * TODO: Replace alignment with something non-branching like:
	 *  uptr pagemask = pagesize - 1;
	 *  uptr capacity = (required + pagemask) & ~pagemask;
	 */
	
	var allocator = pool->allocator;
	var size = initial_size;
	var current = pool->current;
	var alignment = pool->alignment;
	
	assert(alignment <= 128);
	assert(IS_POW2(alignment));
	
	if (current)
	{
		var ptr = (uptr)current->data + current->length;
		var mask = ptr & (alignment - 1);
		if (mask)
			size += alignment - mask;
	}
	
	// Allocated a new block of memory
	if (!current || ((current->length + size) > current->capacity))
	{
		// TODO: Check if in the current block is still some room left to be reused and put next in queue
		// TODO: We should probably have a slow version of allocate: checking all blocks in a pool for available space 
		
		size = initial_size;
		current = allocate(allocator, size);
		current->previous = pool->current;
		pool->current = current;
	}
	
	uptr offset = current->length;
	var ptr = (uptr)current->data + offset;
	var mask = ptr & (alignment - 1);
	if (mask)
		offset += alignment - mask;
	
	return offset;
}

/**
 * Request a buffer with at least the size of _initial_size_.
 * It does not reserve the space - thus be careful not calling this twice before
 * a keep call.
 */
inline buffer request(memory::pool* pool, uptr initial_size)
{
	uptr offset = prepare(pool, initial_size);
	var current = pool->current;
	
	buffer result;
	result.data = current->data + offset;
	result.length = current->capacity - offset;
	return result;
}

inline void keep(memory::pool* pool, uptr initial_size)
{
	pool->current->length += initial_size;
}

/**
 * Flags:
 *  - 1: expand the whole block in the pool
 */
inline buffer allocate(memory::pool* pool, uptr initial_size, flags32 flags = 0)
{
	uptr offset = prepare(pool, initial_size);
	var current = pool->current;
	if (flags) initial_size = current->capacity - offset;
	current->length = offset + initial_size;
	
	buffer result;
	result.data = current->data + offset;
	result.length = initial_size;
	
	return result;
}

inline memory::pool temp(memory::pool p)
{
	memory::pool result = p;
	result.current = null;
	result.alignment = 1;
	return result;
}

/**
 * flattens and recycles the src pool to the dst pool. 
 */
inline buffer flatten(memory::pool* src, memory::pool* dst)
{
	// Calculating the total size of the pool & Reversing the pool
	uptr size = 0;
	{
		memory::managed* current = src->current;
		memory::managed* previous = null;
		while (current)
		{
			size += current->length;
			
			var next = current->previous;
			current->previous = previous;
			previous = current;
			current = next;
		}
		src->current = previous;
	}
	
	
	var result = allocate(dst, size);
	{
		var allocator = src->allocator;
		var data = result.data;
		var current = src->current;
		var offset = 0;
		while (current)
		{
			var length = current->length;
			var current_data = current->data;
			for (uptr i = 0; i < length; ++i)
				data[i + offset] = current_data[i];
					
			offset += length;
			var previous = current->previous;
			recycle(allocator, current);
			current = previous;
		}
	}
	
	return result;
}

inline void insert(memory::pool* pool, memory::managed* data)
{
	var current = pool->current;
	if (current)
	{
		data->previous = current->previous;
		current->previous = data;
	}
	else
	{
		current = data;
		pool->current = current;
	}
}

inline buffer append(memory::pool* pool, buffer data)
{
	buffer result = allocate(pool, data.length);
	copy(result.data, data.data, data.length);
	return result;
}

inline buffer append(memory::pool* pool, byte data)
{
	buffer result = allocate(pool, 1);
	result.data[0] = data;
	return result;
}

template<typename T>
inline buffer append(memory::pool* pool, T data)
{
	buffer result = allocate(pool, sizeof(T));
	((T*)result.data)[0] = data;
	return result;
}

template<typename T, uptr N>
inline buffer append(memory::pool* pool, T data[N])
{
	buffer result = allocate(pool, N * sizeof(T));
	for (var i = 0; i < N; i++)
		((T*)result.data)[i] = data[i];
		
	return result;
}

/* DEPRECATED: Flatten into itsself
inline void flatten(memory::pool* pool)
{
	// Most common check
	{
		memory::managed* current = pool->current;
		if (!current || !current->previous)
			return;
	}
	
	// Calculating the total size of the pool & Reversing the pool
	uptr size = 0;
	{
		memory::managed* current = pool->current;
		memory::managed* previous = null;
		while (current)
		{
			size += current->length;
			
			var next = current->previous;
			current->previous = previous;
			previous = current;
			current = next;
		}
		pool->current = previous;
	}
	
	// Finding the best fit
	memory::managed* fit = null;
	uptr fit_offset;
	{
		memory::managed* current = pool->current;
		uptr fit_min = UPTRMAX;
		uptr fit_size = 0;
		while (current)
		{
			var capacity = current->capacity;
			if (capacity < fit_min && capacity >= size)
			{
				fit = current;
				fit_min = capacity;
				fit_offset = fit_size;
			}
			
			fit_size += current->length;
			current = current->previous;
		}
	}
	
	// Actual flattening
	if (fit)
	{
		var data = fit->data;
			
		// Move best fit block data to correct position
		if (fit_offset)
		{
			uptr fit_length = fit->length;
			for (uptr i = fit_length - 1; i < fit_length; --i)
				data[i + fit_offset] = data[i];
		}
		
		// Copy en recycle all other blocks to the best fit block
		fit_offset = 0;
		
		var allocator = pool->allocator;
		var current = pool->current;
		while (current)
		{
			var previous = current->previous;
			var length = current->length;
			if (current != fit)
			{
				var current_data = current->data;
				for (uptr i = 0; i < length; ++i)
					data[i + fit_offset] = current_data[i];
				
				recycle(allocator, current);
			}
			
			fit_offset += length;
			current = previous;
		}
		
		fit->length = size;
	}
	else
	{
		var allocator = pool->allocator;
		fit = allocate(allocator, size);
		fit->length = size;
		fit_offset = 0;
		
		var data = fit->data;
		var current = pool->current;
		while (current)
		{
			var length = current->length;
			var current_data = current->data;
			for (uptr i = 0; i < length; ++i)
				data[i + fit_offset] = current_data[i];
					
			fit_offset += length;
			var previous = current->previous;
			recycle(allocator, current);
			current = previous;
		}
	}
	
	pool->current = fit;
	fit->previous = null;
}*/

