
memory::unmanaged allocate(uptr pagesize, uptr required, flags32 flags)
{
	assert(IS_POW2(pagesize));
	
	uptr pagemask = pagesize - 1;
	uptr capacity = (required + pagemask) & ~pagemask;
	memory::unmanaged result { };
	
	#if PLATFORM_WIN
	{
		result.capacity = capacity;
		result.data = (byte*)VirtualAlloc(0, capacity, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	}
	#elif PLATFORM_UNIX || PLATFORM_WASM // usr/include/sys/mman.h
	{
		result.capacity = capacity;
		
		int p = 0x01 | 0x02; // PROT_READ | PROT_WRITE
		int f = 0;
		
		// Sharing
		if (flags & memory::SHARED)
		{
			// MAP_SHARED
			f |= 0x0001;
			result.flags |= memory::SHARED;
		}
		else
		{
			// MAP_PRIVATE
			f |= 0x0002;
		}
		
		// MAP_ANONYMOUS (this will initialize data to 0)
		#if PLATFORM_BSD
		f |= 0x1000;
		#elif PLATFORM_LINUX || PLATFORM_ANDROID || PLATFORM_WASM
		f |= 0x0020;
		#else
			#error Unsupported platform!
		#endif
		
		result.capacity = capacity;
		result.data = (byte*)mmap(0, capacity, p, f, -1, 0);
	}
	#else
		#error Unsupported platform!
	#endif
	
	return result;
}

void deallocate(void* block)
{
	#if PLATFORM_WIN
		VirtualFree(block, 0, MEM_RELEASE);
	#elif PLATFORM_UNIX || PLATFORM_WASM || PLATFORM_WASM
		munmap(block, 0);
	#else
		#error Unsupported platform!
	#endif
}

inline void deallocate(buffer m) { deallocate(m.data); }
