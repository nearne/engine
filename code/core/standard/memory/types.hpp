/**
 * USAGE:
 * 
 * 
 */
namespace memory
{
	constexpr flags32 NONE              = 0;
	constexpr flags32 EXHAUSTED         = 1 << 1;
	constexpr flags32 CLEAR             = 1 << 2; // clear on allocation
	constexpr flags32 TEMP              = 1 << 3;
	constexpr flags32 SHARED            = 1 << 4; // accessible on different threads/forks
	constexpr flags32 COPY              = 1 << 6;
	constexpr flags32 BOUNDS_CHECK      = 1 << 7; // add protective padding to allocation
	
	constexpr flags32 DEFAULT_FLAGS     = NONE;
	constexpr flags32 DEFAULT_ALIGNMENT = 64;
	constexpr uptr DEFAULT_TEMP_CAPACITY = 2 * DEFAULT_ALIGNMENT;
	
	constexpr flags32 JOB_FREE          = 0;
	constexpr flags32 JOB_WORKING       = 1 << 0;
	constexpr flags32 JOB_DONE          = 1 << 1;
	
	/**
	 * Block of memory - for internal use.
	 * It is used as a base type to share dynamic buffer functionality:
	 *  - appending data
	 */
	struct block : public buffer
	{
		uptr capacity;
		flags32 flags;
	};
	
	/**
	 * Unmanaged dynamic buffer type:
	 *  - Use with care - try to use managed data
	 *  - The owner is responsible for not sharing the data.
	 *  - Useful for shared memory operations
	 */
	struct unmanaged : public block { };
	
	/**
	 * Managed dynamic buffer type:
	 *  - Used in conjuction with memory::allocator
	 *  - When the content is overflowing a new block will be allocated.
	 *  - Block data is stored on the (beginning of the) allocated data.
	 *  - Appended data is meant to be permanent until the allocator will reset
	 *    all data.
	 */
	struct managed : public block
	{
		struct managed* previous;  // Previous block in the pool - mutable
		struct managed* _previous; // Platform memory map previous - mostly immutable
		struct managed* _next;     // Platform memory map next - mostly immutable
	};
	
	/**
	 * Manages managed memory:
	 *  - Allocates new blocks when previous are full.
	 */
	struct allocator
	{
		struct managed* current;
		struct managed* bin; // empty recyclable blocks
		struct managed* temp; // 
		struct mutex mutex;
		uptr pagesize;
		uptr min;
		flags32 flags;
	};
	
	/**
	 * userspace allocator:
	 * 
	 *  - Can also be used a temporary memory: create a pool, write an uncertain
	 *    amount of bytes. Flatten the pool and move it to the temporary pool - 
	 *    or recycle the pool if not needed.
	 */
	struct pool
	{
		struct allocator* allocator;
		struct managed* current;
		int alignment;
	};


	/**
	 * Statistics of a pool/allocator.
	 * str(stats) is located in code/standard/io/str.hpp
	 */
	struct stats
	{
		uptr blocks;      // Total managed blocks alive
		uptr used;        // Total bytes alive
		uptr fragmented;  // Total estimate fragmented bytes
		uptr available;   // Total bytes free
	};
	
	/**
	 * TODO: ---
	 * 
	 * 
	 */
	struct job
	{
		flags32 flags;
		struct allocator* allocator;
		struct pool* temp;
	};
};

memory::stats stats(memory::pool pool)
{
	memory::stats result {};
	
	var head = pool.current;
	var current = head;
	while (current)
	{
		result.blocks++;
		if (current->length)
		{
			result.used += current->length;
			if (current != head)
				result.fragmented += current->capacity - current->length;
		}
		else
		{
			result.available += current->capacity;
		}
		current = current->previous;
	}

	return result;
}

memory::stats stats(memory::allocator allocator)
{
	memory::stats result {};
	
	var head = allocator.current;
	var current = head;
	do
	{
		result.blocks++;
		if (current->length)
		{
			result.used += current->length;
			if (current != head)
				result.fragmented += current->capacity - current->length;
		}
		else
		{
			result.available += current->capacity;
		}
		current = current->_next;
	} while (current != head);

	return result;
}


