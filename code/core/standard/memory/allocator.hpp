memory::managed* allocate(struct memory::allocator* allocator, uptr required)
{
	memory::managed* result;
	flags32 flags = allocator->flags;
	uptr pagesize = allocator->pagesize;
	uptr pagemask = pagesize - 1;
	memory::managed* bin = allocator->bin;
	
	assert(IS_POW2(allocator->pagesize));
	
	// Recycle discarded memory (TODO: Should we do a search?)
	if (bin && required <= bin->capacity)
	{
		result = bin;
		allocator->bin = bin->previous;
		allocator->current = result;
		return result;
	}
	
	// When needing new memory make sure we reserve at least some workable amount
	if (required < allocator->min)
		required = allocator->min;
	
	#if PLATFORM_WIN
	{
		#error Not implemented!
	}
	#elif PLATFORM_UNIX || PLATFORM_WASM
	{
		int p = 0x01 | 0x02; // PROT_READ | PROT_WRITE
		int f = 0;
		
		// Sharing
		if (flags & memory::SHARED)
			f |= 0x0001; // MAP_SHARED
		
		// MAP_ANONYMOUS (this will initialize data to 0)
		#if PLATFORM_BSD
		f |= 0x1000;
		#elif PLATFORM_LINUX || PLATFORM_ANDROID || PLATFORM_WASM
		f |= 0x0020;
		#else
			#error Unsupported platform!
		#endif
		
		flags32 flags = allocator->flags;
		if (flags & memory::BOUNDS_CHECK)
		{
			uptr protection_range = 2 * ((1024 + pagemask) & ~pagemask);
			uptr header_size = protection_range + ((sizeof(memory::managed) + pagemask) & ~pagemask);
			uptr footer_size = protection_range;
			uptr body_size = (required + pagemask) & ~pagemask;
			uptr total_size = header_size + footer_size + body_size;
			var data = (byte*)mmap(0, total_size, p, f, -1, 0);
			
			int success;
			success = mprotect(data + (header_size - protection_range), protection_range, 0);
			assert(success == 0);
			success = mprotect(data + (total_size - protection_range), protection_range, 0);
			assert(success == 0);
			
			result = (memory::managed*)data;
			result->capacity = body_size;
			result->data = data + header_size;
		}
		else
		{
			uptr total_required = sizeof(memory::managed) + required;
			uptr total_size = (total_required + pagemask) & ~pagemask;
			var data = (byte*)mmap(0, total_size, p, f, -1, 0);
			
			result = (memory::managed*)data;
			result->capacity = total_size - sizeof(memory::managed);
			result->data = data + sizeof(memory::managed);
		}
		
		result->length = 0;
		result->flags = 0;
	}
	#else
		#error Unsupported platform!
	#endif
	
	lock(&allocator->mutex);
	{
		var current = allocator->current;
		result->_next = current;
		result->_previous = current->_previous;
		result->_previous->_next = result;
		result->_next->_previous = result;
		allocator->current = result;
	}
	unlock(&allocator->mutex);
	
	return result;
}

/**
 * Places block in the recycling bin ready to be reused.
 * Probably no need to be ordered - to increase the likelyhood of using bigger blocks first.
 */
void recycle(struct memory::allocator* allocator, memory::managed* block)
{
	block->length = 0;
	block->previous = allocator->bin;
	allocator->bin = block;
}

/**
 * Should be used sparsely: try to recycle allocated memory
 */
void deallocate(struct mutex* mutex, memory::managed* block)
{
	lock(mutex);
	{
		block->_previous->_next = block->_next;
		block->_next->_previous = block->_previous;
	}
	unlock(mutex);
	
	#if PLATFORM_WIN
		#error Not Implemented!
	#elif PLATFORM_UNIX || PLATFORM_WASM
		munmap(block, 0);
	#else
		#error Unsupported platform!
	#endif
}
