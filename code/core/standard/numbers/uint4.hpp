
union uint4
{
	struct
	{
		union
		{
			struct { uint x, y; };
			struct { uint r, g; };
			uint2 xy;
			uint2 rg;
		};
		union
		{
			struct { uint z, w; };
			struct { uint b, a; };
			uint2 zw;
			uint2 ba;
		};
	};
	struct
	{
		uint __0;
		union
		{
			uint2 yz;
			uint2 gb;
		};
		uint __1;
	};
	struct
	{
		union
		{
			uint3 xyz;
			uint3 rgb;
		};
		uint __2;
	};
	struct
	{
		uint __3;
		union
		{
			uint3 yzw;
			uint3 gba;
		};
	};
	uint e[4];
};
