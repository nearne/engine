union float3
{
	struct
	{
		union
		{
			float2 xy;
			struct { float x, y; };
		};
		float z;
	};
	struct { float __0; float2 yz; };
	struct { float r, g, b; };
	float e[3];
	
	static const float3 zero;
	static const float3 one;
	static const float3 left;
	static const float3 right;
	static const float3 down;
	static const float3 up;
	static const float3 backward;
	static const float3 forward;
};
const float3 float3::zero     = { 0.0f, 0.0f, 0.0f };
const float3 float3::one      = { 1.0f, 1.0f, 1.0f };
const float3 float3::left     = {-1.0f, 0.0f, 0.0f };
const float3 float3::right    = { 1.0f, 0.0f, 0.0f };
const float3 float3::down     = { 0.0f,-1.0f, 0.0f };
const float3 float3::up       = { 0.0f, 1.0f, 0.0f };
const float3 float3::backward = { 0.0f, 0.0f,-1.0f };
const float3 float3::forward  = { 0.0f, 0.0f, 1.0f };

inline float3 operator - (float3 l) { return { -l.x, -l.y, -l.z }; }
inline float3 operator - (float3 l, float3 r) { return { l.x - r.x, l.y - r.y, l.z - r.z }; }
inline float3 operator - (float r, float3 l) { return { r - l.x, r - l.y, r - l.z }; }
inline float3 operator - (float3 l, float r) { return { l.x - r, l.y - r, l.z - r }; }
inline float3 operator + (float3 l, float3 r) { return { l.x + r.x, l.y + r.y, l.z + r.z }; }
inline float3 operator + (float r, float3 l) { return { r + l.x, r + l.y, r + l.z }; }
inline float3 operator + (float3 l, float r) { return { l.x + r, l.y + r, l.z + r }; }
inline float3 operator * (float3 l, float3 r) { return { l.x * r.x, l.y * r.y, l.z * r.z }; }
inline float3 operator * (float l, float3 r) { return { l * r.x, l * r.y, l * r.z }; }
inline float3 operator * (float3 l, float r) { return { l.x * r, l.y * r, l.z * r }; }
inline float3 operator / (float3 l, float r) { return { l.x / r, l.y / r, l.z / r }; }

inline float3& operator -= (float3& l, float3 r)
{
	l.x -= r.x;
	l.y -= r.y;
	l.z -= r.z;
	return l;
}
inline float3& operator -= (float3& l, float r)
{
	l.x -= r;
	l.y -= r;
	l.z -= r;
	return l;
}
inline float3& operator += (float3& l, float3 r)
{
	l.x += r.x;
	l.y += r.y;
	l.z += r.z;
	return l;
}
inline float3& operator += (float3& l, float r)
{
	l.x += r;
	l.y += r;
	l.z += r;
	return l;
}
inline float3& operator *= (float3& l, float3 r)
{
	l.x *= r.x;
	l.y *= r.y;
	l.z *= r.z;
	return l;
}
inline float3& operator *= (float3& l, float r)
{
	l.x *= r;
	l.y *= r;
	l.z *= r;
	return l;
}
inline float3& operator /= (float3& l, float r)
{
	l.x /= r;
	l.y /= r;
	l.z /= r;
	return l;
}


inline float3 modf(float3 l, float3* r) { return { modf(l.x, &r->x), modf(l.y, &r->y), modf(l.z, &r->z) }; }

inline float3 clamp(float3 x, float3 min, float3 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
		x.z < min.z ? min.z : x.z > max.z ? max.z : x.z,
	};
}
inline float distance(float3 l, float3 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	float dz = l.z - r.z;
	return sqrt(dx * dx + dy * dy + dz * dz);
}
inline float dot(float3 l, float3 r) { return l.x * r.x + l.y * r.y + l.z * r.z; }
inline float3 faceforward(float3 n, float3 i, float3 g) { return dot(i, g) < 0 ? n : -n; }
inline float3 fixnan(float3 x, float3 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y, isnan(x.z) ? value.z : x.z }; }
inline float length(float3 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z); }
inline float3 lerp(float3 l, float3 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t), lerp(l.z, r.z, t) }; }
inline float3 lerps(float3 l, float3 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t), lerps(l.z, r.z, t) }; }
inline float magnitude(float3 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z); }
inline float3 max(float3 l, float3 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y, l.z > r.z ? l.z : r.z }; }
inline float3 min(float3 l, float3 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y, l.z < r.z ? l.z : r.z }; }

inline float3 normalize(float3 v)
{
	auto length = v.x * v.x + v.y * v.y + v.z * v.z;
	if (length < EPSILON)
		return { 0.0f, 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length, v.z * length };
}
inline float3 reflect(float3 direction, float3 normal) { return -2.0f * dot(direction, normal) * normal + direction; }

inline float3 refract(float3 i, float3 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float3{};
}
inline float3 smoothstep(float3 min, float3 max, float3 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y), smoothstep(min.z, max.z, x.z) }; }
inline float sqrlength(float3 x) { return x.x * x.x + x.y * x.y + x.z * x.z; }
inline float sqrmagnitude(float3 x) { return x.x * x.x + x.y * x.y + x.z * x.z; }




// Special math
inline float3 cross(float3 lhs, float3 rhs)
{
	return {
		lhs.y * rhs.z - lhs.z * rhs.y,
		lhs.z * rhs.x - lhs.x * rhs.z,
		lhs.x * rhs.y - lhs.y * rhs.x
	};
}
//inline float3 project(float3 a, float3 b) { return (b * (dot(a, b) / dot(b, b))); }
//inline float3 reject(float3 a, float3 b) { return (a - b * (dot(a, b) / dot(b, b))); }



#define float3_comp_proc(proc) inline float3 proc(float3 v) {\
	return { proc(v.x), proc(v.y), proc(v.z) };\
}
#define float3_comp_proc_vec1(proc) inline float3 proc(float3 v, float r) {\
	return { proc(v.x, r), proc(v.y, r), proc(v.z, r) };\
}
#define float3_comp_proc_vec3(proc) inline float3 proc(float3 v, float3 r) {\
	return { proc(v.x, r.x), proc(v.y, r.y), proc(v.z, r.z) };\
}

float3_comp_proc(abs)
float3_comp_proc(acos)
float3_comp_proc(asin)
float3_comp_proc(atan)
float3_comp_proc_vec1(atan2)
float3_comp_proc_vec3(atan2)
float3_comp_proc(ceil)
float3_comp_proc(cbrt)
float3_comp_proc(cos)
float3_comp_proc(cosh)
float3_comp_proc(degrees)
float3_comp_proc(exp)
float3_comp_proc(exp2)
float3_comp_proc(floor)
float3_comp_proc_vec1(fmod)
float3_comp_proc_vec3(fmod)
float3_comp_proc(frac)
float3_comp_proc(log)
float3_comp_proc(log10)
float3_comp_proc(log2)
float3_comp_proc_vec1(pow)
float3_comp_proc_vec3(pow)
float3_comp_proc(radians)
float3_comp_proc(round)
float3_comp_proc(rsqrt)
float3_comp_proc(saturate)
float3_comp_proc(sin)
float3_comp_proc(sinh)
float3_comp_proc(sqrt)
float3_comp_proc(tan)
float3_comp_proc(tanh)
float3_comp_proc(trunc)
