
union uint3
{
	struct
	{
		union
		{
			uint2 xy;
			struct { uint x, y; };
		};
		uint z;
	};
	struct { uint __0; uint2 yz; };
	struct { uint r, g, b; };
	uint e[3];
};
