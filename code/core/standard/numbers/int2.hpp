
union int2
{
	struct { int x, y; };
	int e[2];
};
constexpr int hashcode(int2 v) { return v.x + v.y * 0xBD4BCB5; }

inline int2 operator - (int2 l) { return { -l.x, -l.y }; }
inline int2 operator - (int2 l, int2 r) { return { l.x - r.x, l.y - r.y }; }
inline int2 operator - (int r, int2 l) { return { r - l.x, r - l.y }; }
inline int2 operator - (int2 l, int r) { return { l.x - r, l.y - r }; }
inline int2 operator + (int2 l, int2 r) { return { l.x + r.x, l.y + r.y }; }
inline int2 operator + (int r, int2 l) { return { r + l.x, r + l.y }; }
inline int2 operator + (int2 l, int r) { return { l.x + r, l.y + r }; }
inline int2 operator * (int2 l, int2 r) { return { l.x * r.x, l.y * r.y }; }
inline int2 operator * (int l, int2 r) { return { l * r.x, l * r.y }; }
inline int2 operator * (int2 l, int r) { return { l.x * r, l.y * r }; }

inline int2& operator -= (int2& l, int2 r)
{
	l.x -= r.x;
	l.y -= r.y;
	return l;
}
inline int2& operator -= (int2& l, int r)
{
	l.x -= r;
	l.y -= r;
	return l;
}
inline int2& operator += (int2& l, int2 r)
{
	l.x += r.x;
	l.y += r.y;
	return l;
}
inline int2& operator += (int2& l, int r)
{
	l.x += r;
	l.y += r;
	return l;
}
inline int2& operator *= (int2& l, int2 r)
{
	l.x *= r.x;
	l.y *= r.y;
	return l;
}
inline int2& operator *= (int2& l, int r)
{
	l.x *= r;
	l.y *= r;
	return l;
}
