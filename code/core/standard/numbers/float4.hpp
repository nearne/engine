union float4
{
	struct
	{
		union
		{
			struct { float x, y; };
			struct { float r, g; };
			float2 xy;
			float2 rg;
		};
		union
		{
			struct { float z, w; };
			struct { float b, a; };
			float2 zw;
			float2 ba;
		};
	};
	struct
	{
		float __0;
		union
		{
			float2 yz;
			float2 gb;
		};
		float __1;
	};
	struct
	{
		union
		{
			float3 xyz;
			float3 rgb;
		};
		float __2;
	};
	struct
	{
		float __3;
		union
		{
			float3 yzw;
			float3 gba;
		};
	};
	float e[4];
	
	static const float4 zero;
	static const float4 one;
};
const float4 float4::zero = { 0.0f, 0.0f, 0.0f, 0.0f };
const float4 float4::one  = { 1.0f, 1.0f, 1.0f, 1.0f };

inline float4 operator - (float4 l)           { return { -l.x, -l.y, -l.z, -l.w }; }
inline float4 operator - (float4 l, float4 r) { return { l.x - r.x, l.y - r.y, l.z - r.z, l.w - r.w }; }
inline float4 operator - (float  r, float4 l) { return { r   - l.x, r   - l.y, r   - l.z, r   - l.w }; }
inline float4 operator - (float4 l, float  r) { return { l.x -   r, l.y - r  , l.z - r  , l.w - r   }; }
inline float4 operator + (float4 l, float4 r) { return { l.x + r.x, l.y + r.y, l.z + r.z, l.w + r.w }; }
inline float4 operator + (float  r, float4 l) { return { r   + l.x, r   + l.y, r   + l.z, r   + l.w }; }
inline float4 operator + (float4 l, float  r) { return { l.x +   r, l.y + r  , l.z + r  , l.w + r   }; }
inline float4 operator * (float4 l, float4 r) { return { l.x * r.x, l.y * r.y, l.z * r.z, l.w * r.w }; }
inline float4 operator * (float  l, float4 r) { return { l   * r.x, l   * r.y, l   * r.z, l   * r.w }; }
inline float4 operator * (float4 l, float  r) { return { l.x *   r, l.y * r  , l.z * r  , l.w * r   }; }
inline float4 operator / (float4 l, float  r) { return { l.x /   r, l.y / r  , l.z / r  , l.w / r   }; }

inline float4& operator -= (float4& l, float4 r)
{
	l.x -= r.x;
	l.y -= r.y;
	l.z -= r.z;
	l.w -= r.w;
	return l;
}
inline float4& operator -= (float4& l, float r)
{
	l.x -= r;
	l.y -= r;
	l.z -= r;
	l.w -= r;
	return l;
}
inline float4& operator += (float4& l, float4 r)
{
	l.x += r.x;
	l.y += r.y;
	l.z += r.z;
	l.w += r.w;
	return l;
}
inline float4& operator += (float4& l, float r)
{
	l.x += r;
	l.y += r;
	l.z += r;
	l.w += r;
	return l;
}
inline float4& operator *= (float4& l, float4 r)
{
	l.x *= r.x;
	l.y *= r.y;
	l.z *= r.z;
	l.w *= r.w;
	return l;
}
inline float4& operator *= (float4& l, float r)
{
	l.x *= r;
	l.y *= r;
	l.z *= r;
	l.w *= r;
	return l;
}
inline float4& operator /= (float4& l, float r)
{
	l.x /= r;
	l.y /= r;
	l.z /= r;
	l.w /= r;
	return l;
}

inline float4 modf(float4 l, float4* r) { return { modf(l.x, &r->x), modf(l.y, &r->y), modf(l.z, &r->z), modf(l.w, &r->w) }; }

inline float4 clamp(float4 x, float4 min, float4 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
		x.z < min.z ? min.z : x.z > max.z ? max.z : x.z,
		x.w < min.w ? min.w : x.w > max.w ? max.w : x.w,
	};
}
inline float distance(float4 l, float4 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	float dz = l.z - r.z;
	float dw = l.w - r.w;
	return sqrt(dx * dx + dy * dy + dz * dz + dw * dw);
}
inline float dot(float4 l, float4 r) { return l.x * r.x + l.y * r.y + l.z * r.z + l.w * r.w; }
inline float4 faceforward(float4 n, float4 i, float4 g) { return dot(i, g) < 0 ? n : -n; }
inline float4 fixnan(float4 x, float4 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y, isnan(x.z) ? value.z : x.z, isnan(x.w) ? value.w : x.w }; }
inline float length(float4 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w); }
inline float4 lerp(float4 l, float4 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t), lerp(l.z, r.z, t), lerp(l.w, r.w, t) }; }
inline float4 lerps(float4 l, float4 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t), lerps(l.z, r.z, t), lerps(l.w, r.w, t) }; }
inline float magnitude(float4 x) { return sqrt(x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w); }
inline float4 max(float4 l, float4 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y, l.z > r.z ? l.z : r.z, l.w > r.w ? l.w : r.w }; }
inline float4 min(float4 l, float4 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y, l.z < r.z ? l.z : r.z, l.w < r.w ? l.w : r.w }; }

inline float4 normalize(float4 v)
{
	auto length = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	if (length < EPSILON)
		return { 0.0f, 0.0f, 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length, v.z * length, v.w * length };
}
inline float4 reflect(float4 direction, float4 normal) { return -2.0f * dot(direction, normal) * normal + direction; }

inline float4 refract(float4 i, float4 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float4{};
}
inline float4 smoothstep(float4 min, float4 max, float4 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y), smoothstep(min.z, max.z, x.z), smoothstep(min.w, max.w, x.w) }; }
inline float sqrlength(float4 x) { return x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w; }
inline float sqrmagnitude(float4 x) { return x.x * x.x + x.y * x.y + x.z * x.z + x.w * x.w; }


#define float4_comp_proc(proc) inline float4 proc(float4 v) {\
	return { proc(v.x), proc(v.y), proc(v.z), proc(v.w) };\
}
#define float4_comp_proc_vec1(proc) inline float4 proc(float4 v, float r) {\
	return { proc(v.x, r), proc(v.y, r), proc(v.z, r), proc(v.w, r) };\
}
#define float4_comp_proc_vec4(proc) inline float4 proc(float4 v, float4 r) {\
	return { proc(v.x, r.x), proc(v.y, r.y), proc(v.z, r.z), proc(v.w, r.w) };\
}

float4_comp_proc(abs)
float4_comp_proc(acos)
float4_comp_proc(asin)
float4_comp_proc(atan)
float4_comp_proc_vec1(atan2)
float4_comp_proc_vec4(atan2)
float4_comp_proc(ceil)
float4_comp_proc(cbrt)
float4_comp_proc(cos)
float4_comp_proc(cosh)
float4_comp_proc(degrees)
float4_comp_proc(exp)
float4_comp_proc(exp2)
float4_comp_proc(floor)
float4_comp_proc_vec1(fmod)
float4_comp_proc_vec4(fmod)
float4_comp_proc(frac)
float4_comp_proc(log)
float4_comp_proc(log10)
float4_comp_proc(log2)
float4_comp_proc_vec1(pow)
float4_comp_proc_vec4(pow)
float4_comp_proc(radians)
float4_comp_proc(round)
float4_comp_proc(rsqrt)
float4_comp_proc(saturate)
float4_comp_proc(sin)
float4_comp_proc(sinh)
float4_comp_proc(sqrt)
float4_comp_proc(tan)
float4_comp_proc(tanh)
float4_comp_proc(trunc)
