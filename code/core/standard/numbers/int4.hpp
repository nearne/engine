
union int4
{
	struct
	{
		union
		{
			struct { int x, y; };
			struct { int r, g; };
			int2 xy;
			int2 rg;
		};
		union
		{
			struct { int z, w; };
			struct { int b, a; };
			int2 zw;
			int2 ba;
		};
	};
	struct
	{
		int __0;
		union
		{
			int2 yz;
			int2 gb;
		};
		int __1;
	};
	struct
	{
		union
		{
			int3 xyz;
			int3 rgb;
		};
		int __2;
	};
	struct
	{
		int __3;
		union
		{
			int3 yzw;
			int3 gba;
		};
	};
	int e[4];
};

