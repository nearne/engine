/**
 * This should in most cases only be used as a storage type
 * CPUs are generally not that fast computing shorts
 */

union short2
{
	struct { short x, y; };
	short e[2];
	int i;
};
constexpr int hashcode(short2 v) { return (int)v.x + (int)v.y * 0xBD4BCB5; }

inline int2 operator - (short2 l) { return { -l.x, -l.y }; }
inline int2 operator - (short2 l, short2 r) { return { l.x - r.x, l.y - r.y }; }
inline int2 operator - (int r, short2 l) { return { r - l.x, r - l.y }; }
inline int2 operator - (short2 l, int r) { return { l.x - r, l.y - r }; }
inline int2 operator + (short2 l, short2 r) { return { l.x + r.x, l.y + r.y }; }
inline int2 operator + (int r, short2 l) { return { r + l.x, r + l.y }; }
inline int2 operator + (short2 l, int r) { return { l.x + r, l.y + r }; }
inline int2 operator * (short2 l, short2 r) { return { l.x * r.x, l.y * r.y }; }
inline int2 operator * (int l, short2 r) { return { l * r.x, l * r.y }; }
inline int2 operator * (short2 l, int r) { return { l.x * r, l.y * r }; }
