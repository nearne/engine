// layout - Row Major - m[row][col]
//   |   0     1     2
// --+------------------
// 0 | 0:m00 1:m01 2:m02
// 1 | 3:m10 4:m11 5:m12
// 2 | 6:m20 7:m21 8:m22
union float3x3
{
	struct
	{
		float m00, m01, m02;
		float m10, m11, m12;
		float m20, m21, m22;
	};

	float m[3][3];
	float e[9];
	float3 columns[3];
	
	static const float3x3 zero;
	static const float3x3 identity;
};
const float3x3 float3x3::zero = float3x3 {
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
};
const float3x3 float3x3::identity = float3x3 {
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f,
};

inline float3 operator * (float3x3 l, float3 r)
{
	return {
		l.m00 * r.x + l.m01 * r.y + l.m02 * r.z,
		l.m10 * r.x + l.m11 * r.y + l.m12 * r.z,
		l.m20 * r.x + l.m21 * r.y + l.m22 * r.z,
	};
}


inline float3x3 transpose(float3x3 x)
{
	return {
		x.m00, x.m10, x.m20,
		x.m01, x.m11, x.m21,
		x.m02, x.m12, x.m22,
	};
}

inline float determinant(float3x3 m)
{
	return
		+ m.m00 * (m.m11 * m.m22 - m.m21 * m.m12)
		- m.m01 * (m.m10 * m.m22 - m.m20 * m.m12)
		+ m.m02 * (m.m10 * m.m21 - m.m20 * m.m11);
}

inline float3x3 inverse(float3x3 m)
{
	var determinant =
		+ m.m00 * (m.m11 * m.m22 - m.m21 * m.m12)
		- m.m01 * (m.m10 * m.m22 - m.m20 * m.m12)
		+ m.m02 * (m.m10 * m.m21 - m.m20 * m.m11);
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		+(m.m11 * m.m22 - m.m12 * m.m21) * inv_determinant,
		-(m.m01 * m.m22 - m.m21 * m.m02) * inv_determinant,
		+(m.m01 * m.m12 - m.m11 * m.m02) * inv_determinant,

		-(m.m10 * m.m22 - m.m12 * m.m20) * inv_determinant,
		+(m.m00 * m.m22 - m.m20 * m.m02) * inv_determinant,
		-(m.m00 * m.m12 - m.m10 * m.m02) * inv_determinant,

		+(m.m10 * m.m21 - m.m20 * m.m11) * inv_determinant,
		-(m.m00 * m.m21 - m.m20 * m.m01) * inv_determinant,
		+(m.m00 * m.m11 - m.m01 * m.m10) * inv_determinant,
	};
}
