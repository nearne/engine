#define EPSILON 0.00001f
#define PI      3.14159265358979323846f   // pi
#define TAU     6.28318530717958647692f   // 2pi
#define DEG2RAD 0.017453292519943295f     // pi/180
#define RAD2DEG 57.29577951308232f        // 180/pi
#define MATH_E  2.71828182845904523536f   // e (euler's number)
#define LOG2E   1.44269504088896340736f   // log2(e)
#define LOG10E  0.434294481903251827651f  // log10(e)
#define LN2     0.693147180559945309417f  // ln(2)
#define LN10    2.30258509299404568402f   // ln(10)
#define SQRT2   1.41421356237309504880f   // sqrt(2)

/**
 * Missing libc-math procedures
 */
inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }


union int_float
{
	float f;
	int i;
	uint u;
	byte b[4];

	inline static constexpr int_float ff(float v)
	{
		int_float r{};
		r.f = v;
		return r;
	};
	inline static constexpr int_float fi(int v)
	{
		int_float r{};
		r.i = v;
		return r;
	};
	inline static constexpr int_float fu(uint v)
	{
		int_float r{};
		r.u = v;
		return r;
	};
	inline static constexpr float utf(uint v)
	{
		int_float r{};
		r.u = v;
		return r.f;
	}
};

inline sbyte  clamp(sbyte  x, sbyte  min, sbyte  max) { return x < min ? min : x > max ? max : x; }
inline short  clamp(short  x, short  min, short  max) { return x < min ? min : x > max ? max : x; }
inline int    clamp(int    x, int    min, int    max) { return x < min ? min : x > max ? max : x; }
inline s64    clamp(s64    x, s64    min, s64    max) { return x < min ? min : x > max ? max : x; }
inline byte   clamp(byte   x, byte   min, byte   max) { return x < min ? min : x > max ? max : x; }
inline ushort clamp(ushort x, ushort min, ushort max) { return x < min ? min : x > max ? max : x; }
inline uint   clamp(uint   x, uint   min, uint   max) { return x < min ? min : x > max ? max : x; }
inline u64    clamp(u64    x, u64    min, u64    max) { return x < min ? min : x > max ? max : x; }
inline float  clamp(float  x, float  min, float  max) { return x < min ? min : x > max ? max : x; }
inline double clamp(double x, double min, double max) { return x < min ? min : x > max ? max : x; }
inline float distance(float l, float r) { return abs(l - r); }
inline float dot(float l, float r) { return l * r; }
inline float fixnan(float x, float value = 0) { return isnan(x) ? value : x; }
inline float length(float x) { return abs(x); }
inline float lerp(float l, float r, float t) { return l + t * (r - l); }
inline float lerps(float l, float r, float t) { return l + saturate(t) * (r - l); }
inline float magnitude(float x) { return abs(x); }
inline float max(float l, float r) { return l > r ? l : r; }
inline int min(int l, int r) { return l < r ? l : r; }
inline uint min(uint l, uint r) { return l < r ? l : r; }
inline float min(float l, float r) { return l < r ? l : r; }
inline float reflect(float direction, float normal) { return -2.0f * dot(direction, normal) * normal + direction; }
inline float smoothstep(float min, float max, float x)
{
	float t = saturate((x - min) / (max - min));
	return t * t * (3.0f - (2.0f * t));
}
inline float sqr(float x) { return x * x; }
inline float sqrlength(float x) { return x * x; }
inline float sqrmagnitude(float x) { return x * x; }
float smoothdamp(float current, float target, float* velocity, float smoothness, float timestep)
{
	var invsmoothness = 2 / smoothness;
	var x = invsmoothness * timestep;
	var exp = 1 / (1 + x + 0.48f * x * x + 0.235f * x * x * x);
	var delta = current - target;
	var temp = (*velocity + invsmoothness * delta) * timestep;
	var output = target + (delta + temp) * exp;
	if (delta < 0 == output > target)
	{
		*velocity = 0;
		return target;
	}
	*velocity = (*velocity - invsmoothness * temp) * exp;
	return output;
}

inline float bilinear(float x, float y, float q00, float q10, float q01, float q11) 
{
    float a = 1 - x;
    float b = 1 - y;
    return 
        q00 * a * b +
        q10 * x * b +
        q01 * a * y +
        q11 * x * y;
}

#define PRIMES_COUNT 72
static const int primes[PRIMES_COUNT]{
	3, 7, 11, 17, 23, 29, 37, 47,
	59, 71, 89, 107, 131, 163, 197, 239,
	293, 353, 431, 521, 631, 761, 919, 1103,
	1327, 1597, 1931, 2333, 2801, 3371, 4049, 4861,
	5839, 7013, 8419, 10103, 12143, 14591, 17519, 21023,
	25229, 30293, 36353, 43627, 52361, 62851, 75431, 90523,
	108631, 130363, 156437, 187751, 225307, 270371, 324449, 389357,
	467237, 560689, 672827, 807403, 968897, 1162687, 1395263, 1674319,
	2009191, 2411033, 2893249, 3471899, 4166287, 4999559, 5999471, 7199369
};
inline int PrimeCeiled(int x)
{
	for (int i = 0; i < PRIMES_COUNT; i++)
	{
		int prime = primes[i];
		if (prime >= x)
			return prime;
	}

	assert(!"x is bigger the primes_max");
	return x | 1;
}
