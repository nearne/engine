
 
 /**
  * Value is in nano-seconds.
  * @Performance @Accuracy: There are probably more efficient and accurate ways
  * of dividing nano-seconds.
  */
struct timestamp
{
	static constexpr s64 nano   = 1;
	static constexpr s64 micro  = 1000000;
	static constexpr s64 milli  = 1000000000;
	static constexpr s64 second = 1000000000000;
	static constexpr s64 minute = 60000000000000;
	static constexpr s64 hour   = 3600000000000000;
	
	s64 value;
	
	inline double ns() { return (double)value / nano;   }
	inline double us() { return (double)value / micro;  }
	inline double ms() { return (double)value / milli;  }
	inline double s()  { return (double)value / second; }
	inline double m()  { return (double)value / minute; }
	inline double h()  { return (double)value / hour;   }
	
	static inline timestamp ns(double t) { return { (s64)(t * nano)   }; }
	static inline timestamp us(double t) { return { (s64)(t * micro)  }; }
	static inline timestamp ms(double t) { return { (s64)(t * milli)  }; }
	static inline timestamp  s(double t) { return { (s64)(t * second) }; }
	static inline timestamp  m(double t) { return { (s64)(t * minute) }; }
	static inline timestamp  h(double t) { return { (s64)(t * hour)   }; }
};


/**
 * !Important: call time::init at startup
 * TODO: Provide a RDTSC procedure. This way it is possible to count high-
 * precision performance instead of seconds.
 */
#if PLATFROM_WIN
struct system_time
{
	LARGE_INTEGER timerate;
	
	system_time() { QueryPerformanceFrequency(&timerate); }
	
	inline timestamp monotonic()
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (timestamp)((1e9 * now.QuadPart) / timerate.QuadPart);
	}
	inline timestamp fast()    { return monotonic(); }
	inline timestamp precise() { return monotonic(); }
	inline timestamp now()     { return monotonic(); }
};


#elif PLATFORM_WASM
struct system_time
{
	u64 rate_monotonic;
	u64 rate_precise;
	u64 rate_fast;
	u64 rate_now;
	
	inline timestamp request(int type, u64 rate)
	{
		u64 t;
		__wasi_clock_time_get(type, rate, &t);
		return { (s64)t * timestamp::nano };
	}
	
	inline timestamp monotonic() { return request(4, rate_monotonic); }
	inline timestamp fast()      { return request(4, rate_precise); }
	inline timestamp precise()   { return request(4, rate_fast); }
	inline timestamp now()       { return request(0, rate_now); }
};
static void init(system_time* st)
{
	__wasi_clock_res_get(4, &st->rate_monotonic);
	__wasi_clock_res_get(4, &st->rate_precise);
	__wasi_clock_res_get(4, &st->rate_fast);
	__wasi_clock_res_get(0, &st->rate_now);
}


#elif PLATFORM_UNIX
struct system_time
{
	struct spec
	{
		platform_long seconds;
		platform_long nanoseconds;
	};
	
	inline timestamp request(int type)
	{
		struct spec spec;
		clock_gettime(type, &spec);
		return { spec.seconds * timestamp::second + spec.nanoseconds * timestamp::nano };
	}
	
	#if PLATFORM_BSD
		inline timestamp monotonic() { return request(4); }
		inline timestamp fast()      { return request(12); }
		inline timestamp precise()   { return request(11); }
		inline timestamp now()       { return request(0); }
	#elif PLATFORM_LINUX || PLATFORM_ANDROID || PLATFORM_WASM
		inline timestamp monotonic() { return request(4); }
		inline timestamp fast()      { return request(4); }
		inline timestamp precise()   { return request(4); }
		inline timestamp now()       { return request(0); }
	#else
		#error Unsupported platform!
	#endif
};


#else
	#error Unsupported platform!
#endif
