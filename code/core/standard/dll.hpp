/**
 * Dynamically linked libraries
 * TODO: load_win and load_unix goal is to prevent janky preprocessor solutions.
 * Although, there are probably better ways to have a chain of fallbacks if
 * a platform does not have the latest DLL.
 */

struct dll
{
	string_slice name;
	libhandle handle;
	
	template<typename T>
	T operator()(T& proc, const char* name)
	{
		#if PLATFORM_WIN
			return (proc = (T)GetProcAddress(handle, name));
		#elif PLATFORM_UNIX
			return (proc = (T)dlsym(handle, name));
		#elif PLATFORM_WASM
			// TODO: This!
			return nullptr;
		#else
			#error Unsupported platform
		#endif
	}
};

void dispose(struct dll* dll)
{
	var handle = dll->handle;
	if (handle)
	{
		#if PLATFORM_WIN
			FreeLibrary(handle);
		#elif PLATFORM_UNIX
			dlclose(handle);
		#elif PLATFORM_WASM
			// TODO: This!
		#else
			#error Unsupported platform
		#endif
	}
	
	dll->name = {};
	dll->handle = {};
}

inline bool load_win(struct dll* dll, const char* name)
{
	#if PLATFORM_WIN
	var result = LoadLibraryA(name);
	if (result)
	{
		dll->name = name;
		dll->handle = result;
		return true;
	}
	#endif
	
	return false;
}

inline bool load_unix(struct dll* dll, const char* name)
{
	#if PLATFORM_UNIX
	var result = dlopen(name, 0x00002 | 0x00000); // now | local
	if (result)
	{
		dll->name = name;
		dll->handle = result;
		return true;
	}
	#endif
	
	return false;
}
