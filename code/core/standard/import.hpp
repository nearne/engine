/**
 * ==== STANDARD LAYER ====
 * The standard lays the foundation of the user-space software.
 *  - Complex/compound numbers
 *  - Hashing
 *  - Memory management
 *  - Printing
 */


// TODO: stacktrace type
// TODO: struct os

#include    "numbers/import.hpp"
#include "primitives/import.hpp"
#include     "memory/import.hpp"
#include         "io/import.hpp"

#include "error.hpp"
#include "dll.hpp"
#include "time.hpp"
#include "random.hpp"
