struct error
{
	static struct memory::allocator allocator;
	static struct memory::pool pool; // data of all formatted log items
	static byte temp[512];
	
	int code;
	string_slice message;
	
	error() = default;
	error(int c, string_slice m) : code(c), message(m) {}

	template<typename T, typename... Args>
	error(int c, const char* const f, T t, Args... args)
	{
		if (pool.allocator)
		{
			recycle(&pool, 12);
			struct fmt fmt(::temp(pool));
			format(&fmt, f, t, args...);
			append(&fmt.writer.pool, (byte)0);
			message = flatten(&fmt.writer.pool, &pool);
			code = c;
		}
		else
		{
			code = c;
			message = buffer(temp);
			struct fmt fmt(message);
			format(&fmt, f, t, args...);
			fmt.writer.buffer[fmt.writer.buffer.cursor] = 0;
			message.length = fmt.writer.buffer.cursor;
		}
	}
	
	operator int() const { return code; }
	
	static const error ok;
	static const error unsupported;
	static const error depricated;
};

const error error::ok = error(0, "OK");
const error error::unsupported = error(0x0F0F0000, "Procedure not implemented or supported.");
const error error::depricated  = error(0x0F0F0001, "Procedure is depricated.");
