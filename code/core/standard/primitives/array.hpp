
template<typename T, uptr N>
struct fixed_array : public fixed_slice<T, N>
{
	uptr length;
};
template<typename T, uptr N> inline uptr len(fixed_array<T, N> a) { return a.length; }
template<typename T, uptr N> inline uptr cap(fixed_array<T, N> a) { return N; }
