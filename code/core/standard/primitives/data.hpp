template<uptr N, class T>
constexpr uptr len(T(&)[N]) { return N; }
constexpr uptr len(int i) { return i; }
inline int len(const char* c)
{
	int result = 0;
	if (!c) return result;
	while(c[result]) ++result;
	return result;
}

template<uptr N, class T>
constexpr uptr cap(T(&)[N]) { return N; }
constexpr uptr cap(int i) { return i; }
inline int cap(const char* c)
{
	int result = 0;
	if (!c) return result;
	while(c[result++]);
	return result;
}


inline void clear(void* ptr, uptr size)
{
	var bytes = (byte*)ptr;
	while (size--) *bytes++ = 0;
}

inline bool bigendian()
{
	union {
		short s;
		byte b[2];
	} v { 0x0102 };
	return v.b[0] == 1;
}

template<typename T>
inline T endian_swap(T v)
{
	union {
		T t;
		byte b[sizeof(T)];
	} x, y { v };
	for (var i = 0; i < sizeof(T); i++)
		x.b[i] = y.b[sizeof(T) - i - 1];
	return x.t;
}


inline void copy(void* dest, const void* source, uptr length)
{
	#if 0
	// https://elixir.bootlin.com/linux/latest/source/lib/string.c#L875
	byte* d;
	byte* s;
	uint* dw = (uint*)dest;
	uint* sw = (uint*)source;
	
	if (!((uptr)dw & 3) && !((uptr)sw & 3))
	{
		for (; len >= 4; len -= 4)
			*dw++ = *sw++;
	}

	d = (byte*)dw;
	s = (byte*)sw;
	for (; len >= 1; len -= 1)
		*d++ = *s++;
	#else
	var d = (byte*)dest;
	var s = (byte*)source;
	while(length--)
		*d++ = *s++;
	#endif
}

/**
 * Little endian and big endian will not produce the same result.
 * Therefore, only use this internally. 
 * https://sites.google.com/site/murmurhash/
 */
inline uint hash(const void* src, uint len, uint seed = 0)
{
	constexpr uint m = 0x5BD1E995;
	constexpr int r = 24;
	const byte* data = (const byte*)src;
	uint h = seed ^ len;
	
	while (len >= 4)
	{
		uint k = *(const uint*)data;
		k *= m;
		k ^= k >> r;
		k *= m;
		h *= m;
		h ^= k;
		data += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: h ^= data[2] << 16;
		case 2: h ^= data[1] << 8;
		case 1: h ^= data[0];
		h *= m;
	}
	
	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;
	
	return h;
}

inline bool equals(const void* lhs, const void* rhs, uptr len)
{
	const byte* l = (const byte*)lhs;
	const byte* r = (const byte*)rhs;
	while (len >= 4)
	{
		uint ld = *(const uint*)l;
		uint rd = *(const uint*)r;
		if (ld != rd)
			return false;

		l += 4;
		r += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: if (l[2] != r[2]) return false;
		case 2: if (l[1] != r[1]) return false;
		case 1: if (l[0] != r[0]) return false;
	}
	
	return true;
}
