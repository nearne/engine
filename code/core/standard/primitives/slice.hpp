template<typename T, uptr N>
struct fixed_slice
{
	T data[N];

	inline T const& operator[](uptr index) const { return data[index]; }
	inline T& operator[](uptr index) { return data[index]; }
	inline T const& operator[](sptr index) const { return data[index]; }
	inline T& operator[](sptr index) { return data[index]; }
};
template<typename T, uptr N> inline uptr len(fixed_slice<T, N> a) { return N; }
template<typename T, uptr N> inline uptr cap(fixed_slice<T, N> a) { return N; }


template<typename T = byte>
struct slice
{
	T* data;
	uptr length;
	
	slice() = default;
	slice(T* c, uptr l)
	{
		this->data = c;
		this->length = l;
	}
	slice(const T* c, uptr l)
	{
		this->data = (T*)c;
		this->length = l;
	}
	slice(const char* c)
	{
		this->data = (T*)c;
		this->length = len(c);
	}
	slice(const slice<T>& b) : data(b.data), length(b.length / sizeof(T)) {
		assert(length * sizeof(T) == b.length);
	}
	template<uptr N> slice(fixed_slice<T, N> b) : data(b.data), length(b.length / sizeof(T)) { }
	template<uptr N> slice(T (&data)[N]) : data(data), length(N) { }
	
	inline T const& operator[](sptr index) const { return data[index]; }
	inline T& operator[](sptr index) { return data[index]; }
	inline T const& operator[](uptr index) const { return data[index]; }
	inline T& operator[](uptr index) { return data[index]; }
};
template<typename T> inline int len(slice<T> s) { return s.length; }
template<typename T> inline int cap(slice<T> s) { return s.length; }
template<typename T> inline void clear(slice<T> b)
{
	var size = b.length;
	while(size--) *b.data++ = 0;
}
template<typename T> inline bool equals(slice<T> a, slice<T> b) { return a.length == b.length && equals(a.data, b.data, a.length); }
