/**
 * False sharing warning: The volatile keyword is added, but is this
 * enough to avoid false sharing. If not, add additional padding between
 * ticket and serving. Additionally this will be probably platform
 * dependent how much padding is needed (e.g. 64 bytes padding etc.)
 */
struct mutex
{
	u64 volatile ticket;
	u64 volatile serving;
};

inline void lock(mutex* m)
{
	var ticket = ATOMIC_ADD(&m->ticket, 1);
	while (ticket != m->serving);
}

inline void unlock(mutex* m)
{
	ATOMIC_ADD(&m->serving, 1);
}
