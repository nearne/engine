struct buffer
{
	byte* data;
	uptr length;
	
	buffer() = default;
	buffer(byte* c, uptr l)
	{
		this->data = c;
		this->length = l;
	}
	buffer(const char* c, uptr l)
	{
		this->data = (byte*)c;
		this->length = l;
	}
	buffer(const char* c)
	{
		this->data = (byte*)c;
		this->length = len(c);
	}
	template<typename T, uptr N> explicit buffer(T v[N])
	{
		this->data = (byte*)v;
		this->length = sizeof(T) * N;
	}
	template<typename T> explicit buffer(T* v, uptr l)
	{
		this->data = (byte*)v;
		this->length = sizeof(T) * l;
	}
	template<typename T> explicit buffer(T* v)
	{
		this->data = (byte*)v;
		this->length = sizeof(T);
	}
	
	inline byte const& operator[](int index) const { return data[index]; }
	inline byte& operator[](int index) { return data[index]; }
	inline byte const& operator[](uptr index) const { return data[index]; }
	inline byte& operator[](uptr index) { return data[index]; }
};


// TODO: Deprecate this!!!! > transform it to a stringbuilder, use memory::block for data stuff
struct buffer_builder : public buffer
{
	uptr capacity;

	buffer_builder() = default;
	
	template<uptr N>
	buffer_builder(byte(&x)[N]) : buffer(x, 0)  { capacity = N; }

	template<typename T>
	explicit buffer_builder(T* v, uptr l, uptr c) : buffer(v, l) {
		this->capacity = sizeof(T) * c;
	}
};
inline void append(buffer_builder* dst, const byte* buffer, uptr length)
{
	assert(dst->length + length < dst->capacity);
	
	var d = dst->data + dst->length;
	dst->length += length;
	while(length--)
		*d++ = *buffer++;
}
//inline void append(buffer_builder* dst, slice v) { append(dst, v.data, v.length); }
inline void append(buffer_builder* dst, const char* buffer, uptr length) { append(dst, (byte*)buffer, length); }
inline void append(buffer_builder* dst, const char* buffer) { append(dst, (byte*)buffer, len(buffer)); }

inline void append(buffer_builder* dst, byte value)
{
	assert(dst->length + 1 < dst->capacity);
	dst->data[dst->length++] = value;
}
inline void append(buffer_builder* dst, char value) { return append(dst, (byte)value); }

inline void terminate(buffer_builder dst)
{
	assert(dst.length < dst.capacity);
	dst.data[dst.length] = 0;
}





