struct fmt
{
	struct writer
	{
		void (*write)(writer*, buffer);
		union
		{
			struct : public buffer
			{
				uptr cursor;
			} buffer;
			memory::pool pool;
			void* handle;
			//memory::pool* pool;
		};
	} writer;
	flags32 flags;      // 
	int preceding;      // How many 0's to prepend a number 4 of 21 gives 0021
	int precision_min;  // How digits to round a number of 4 of 2.34567 gives 2.3457
	int precision_max;  // How digits to round a number of 4 of 2.34567 gives 2.3457
	
	fmt() = default;
	fmt(struct buffer);
	fmt(struct memory::pool);
	fmt to(struct buffer) const;
	fmt to(struct memory::pool) const;

	static const char* numbers;
	static const char* lower_hex;
	static const char* upper_hex;

	static constexpr flags32 SIGN            = 1 <<  2; // Explicit sign
	static constexpr flags32 UPPER           = 1 <<  4; // Use upper case
	static constexpr flags32 SCIENTIFIC      = 1 <<  5; // Scientific notation
	static constexpr flags32 HEX             = 1 <<  6; // Base 16 notation
	static constexpr flags32 OCT             = 1 <<  7; // Base 8 notation
	static constexpr flags32 BIN             = 1 <<  8; // Base 2 noration
	static constexpr flags32 BOOL            = 1 <<  9; // true or false
	static constexpr flags32 ESCAPE          = 1 << 10; // Escape a string
	static constexpr flags32 VALUE           = 1 << 11; // If it is a pointer, dereference if possible
	
	static void write_buffer(struct writer* w, buffer v);
	static void write_file(struct writer* w, buffer v);
	static void write_pool(struct writer* w, buffer v);
};

const char* fmt::numbers = "0123456789"; 
const char* fmt::lower_hex = "0123456789abcdef";
const char* fmt::upper_hex = "0123456789ABCDEF";

void fmt::write_buffer(struct writer* w, buffer v)
{
	var dst = w->buffer;
	uptr cursor = dst.cursor;
	uptr i = 0;
	for (; i < v.length && cursor < dst.length; ++cursor, ++i)
		dst[cursor] = v[i];
	
	w->buffer.cursor = cursor;
	assert(i >= v.length);
}

void fmt::write_file(struct writer* w, buffer v)
{
	fwrite(v.data, v.length, sizeof(byte), w->handle);
	fflush(w->handle);
}

void fmt::write_pool(struct writer* w, buffer v)
{
	append(&w->pool, v);
}

fmt::fmt(struct buffer b)
{
	writer.write = write_buffer;
	writer.buffer.data = b.data;
	writer.buffer.length = b.length;
	writer.buffer.cursor = 0;
	flags = 0;
	preceding = 0;
	precision_min = 0;
	precision_max = 0;
}

fmt::fmt(struct memory::pool p)
{
	writer.write = write_pool;
	writer.pool = p;
	flags = 0;
	preceding = 0;
	precision_min = 0;
	precision_max = 0;
}

fmt fmt::to(struct buffer b) const
{
	fmt result = *this;
	result.writer.write = write_buffer;
	result.writer.buffer.data = b.data;
	result.writer.buffer.length = b.length;
	result.writer.buffer.cursor = 0;
	return result;
}

fmt fmt::to(struct memory::pool p) const
{
	fmt result = *this;
	result.writer.write = write_pool;
	result.writer.pool = p;
	return result;
}
