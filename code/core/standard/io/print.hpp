/**
 * TODO: I am not sure if this is a good idea. Although, for debug purposes a
 * print option without passing the destination argument is easy/nice. Rethink
 * these procedures. First option is to only allow the debug procedures having
 * access to global variables, and explicitly define it is for debugging
 * purposes.
 */


void format(struct fmt* fmt, const char* const f)
{
	const char* traverse = f;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - f;
	if (length > 0)
	{
		::buffer v(f, length);
		write(fmt, v);
	}
	assert(!*traverse);
}

template<typename T, typename... Args>
void format(struct fmt* fmt, const char* const f, T arg, Args... args)
{
	const char* traverse = f;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - f;
	if (length > 0)
	{
		::buffer v(f, length);
		write(fmt, v);
	}
	if (!*traverse)
		return;

	flags32 flags = 0;
	while (*++traverse)
	{
		var c = *traverse;
		switch (c)
		{
			case 'h': flags |= fmt::VALUE | fmt::HEX; goto write_arg;
			case 'H': flags |= fmt::VALUE | fmt::HEX | fmt::UPPER; goto write_arg;
			case 'v': flags |= fmt::VALUE; goto write_arg;
		}
	}
	write_arg:
	traverse++;
	fmt->flags = flags;
	str(fmt, arg);
	exit_flags:
	format(fmt, traverse, args...);
}

template<typename T, typename... Args>
void print(const char* const f, T arg, Args... args)
{
	struct fmt fmt {};
	fmt.writer.write = fmt::write_file;
	fmt.writer.handle = stdout;
	format(&fmt, f, arg, args...);
	writec(&fmt, '\n');
}

template<typename T>
void print(T arg)
{
	struct fmt fmt {};
	fmt.writer.write = fmt::write_file;
	fmt.writer.handle = stdout;
	str(&fmt, arg);
	writec(&fmt, '\n');
}
