inline void write(fmt* fmt, buffer b) { fmt->writer.write(&fmt->writer, b); }

static void writea(fmt* fmt, const char* value) { write(fmt, value); }

static void writec(fmt* fmt, char value)
{
	buffer v(&value, 1);
	return write(fmt, v);
}

static void writeu(fmt* fmt, s64 value)
{
	char buffer[128];
	char* ptr = buffer;
	
	var flags = fmt->flags;
	if (flags & fmt::BOOL)
	{
		auto data = flags & fmt::UPPER
			? (value ? "TRUE" : "FALSE")
			: (value ? "true" : "false");
		return write(fmt, data);
	}
	
	var base = 10;
	if (flags & fmt::HEX) base = 16;
	if (flags & fmt::OCT) base = 8;
	if (flags & fmt::BIN) base = 2;
	
	var numbers = flags & fmt::UPPER
		? fmt::upper_hex
		: fmt::lower_hex;
	if (flags & fmt::SIGN)
		*ptr++ = '+';
	
	var tmp = value;
	do {
		*ptr++ = '0';
		tmp /= base;
	} while(tmp || ptr - buffer < fmt->preceding);
	
	var length = ptr - buffer;
	do {
		var next = value / base;
		*--ptr = numbers[value - next * base];
		value = next;
	} while(value);
	
	::buffer v(buffer, length);
	return write(fmt, v);
}

static void writes(fmt* fmt, s64 value)
{
	char buffer[128];
	char* ptr = buffer;
	
	var flags = fmt->flags;
	if (flags & fmt::BOOL)
	{
		var data = flags & fmt::UPPER
			? (value ? "TRUE" : "FALSE")
			: (value ? "true" : "false");
		return writea(fmt, data);
	}
	
	var base = 10;
	if (flags & fmt::HEX) base = 16;
	if (flags & fmt::OCT) base = 8;
	if (flags & fmt::BIN) base = 2;
	
	var numbers = flags & fmt::UPPER
		? fmt::upper_hex
		: fmt::lower_hex;
	
	if (value < 0)
	{
		*ptr++ = '-';
		value = -value;
	}
	else if (flags & fmt::SIGN)
		*ptr++ = '+';
	
	var tmp = value;
	do {
		*ptr++ = '0';
		tmp /= base;
	} while(tmp || ptr - buffer < fmt->preceding);
	
	var length = ptr - buffer;
	do {
		var next = value / base;
		*--ptr = numbers[value - next * base];
		value = next;
	} while(value);
	
	::buffer v(buffer, length);
	return write(fmt, v);
}

static void writef(struct fmt* fmt, float value)
{
	var flags = fmt->flags;
	var base = 10;
	if (flags & (fmt::HEX | fmt::OCT | fmt::BIN))
	{
		//to_string(fmt, x.u);
		return;// error::unsupported;
	}
	
	if (isnan(value))
		return writea(fmt, "NaN");

	var numbers = fmt::lower_hex;
	if (flags & fmt::UPPER) numbers = fmt::upper_hex;

	union
	{
		float f;
		int i;
	} x {value};
	
	char buffer[64];
	var ptr = buffer;
	short exp2 = (uchar)(x.i >> 23) - SBYTEMAX;
	u64 mantissa = (x.i & 0xFFFFFF) | (1 << 23);
	u64 frac_val = 0;
	u64 int_val = 0;
	int safe_shift = ~exp2;
	u64 safe_mask = ULONGMAX >> (64 - 24 - safe_shift); 

	if (x.i < 0) *ptr++ = '-';
	else if (flags & fmt::SIGN) *ptr++ = '+';

	if (exp2 < -36)
	{
		var preceed = fmt->preceding;
		do *ptr++ = '0';
		while (--preceed > 0);

		var precision = fmt->precision_max;
		if (precision)
		{
			*ptr++ = '.';
			do *ptr++ = '0';
			while (--preceed > 0);
		}

		::buffer v(buffer, ptr - buffer);
		return write(fmt, v);
	}
	else if (isinf(value))
	{
		::buffer v(buffer, ptr - buffer);
		write(fmt, v);
		if (flags & fmt::UPPER)
			return writea(fmt, "INFINITY");
		return writea(fmt, "infinity");
	}
	else
	{
		if (exp2 >= 64) int_val = ULONGMAX;
		else if (exp2 >= 23) int_val = mantissa << (exp2 - 23);
		else if (exp2 >= 0)
		{
			int_val = mantissa >> (23 - exp2);
			frac_val = mantissa & safe_mask;
		}
		else frac_val = mantissa & 0xFFFFFF;

		var preceed = fmt->preceding;
		var int_tmp = int_val;
		do {
			*ptr++ = '0';
			int_tmp /= base;
		} while(int_tmp || ptr - buffer < preceed);
		
		var resume_ptr = ptr;
		var length = ptr - buffer;
		do {
			int_tmp = int_val / base;
			*--ptr = numbers[int_val - int_tmp * base];
			int_val = int_tmp;
		} while(int_val);
		ptr = resume_ptr;
		

		int precision = fmt->precision_max;
		if (frac_val)
		{
			*ptr++ = '.';
			var prec = precision;
			if (!prec)
			{
				prec = 4;
			}
			else if (prec > 8) prec = 8;
			while(--prec > 0)
			{
				frac_val *= base;
				*ptr++ = numbers[frac_val >> (24 + safe_shift)];
				frac_val &= safe_mask;
			}
			// We do not round to safe complexity: 99.9999 or hex values
			if (!precision)	
			{
				while (*(ptr-1) == '0')
				{
					ptr--;
					if (*(ptr-1)=='.')
					{
						ptr--;
						break;
					}
				}
			}
		}
		else if (precision)
		{
			if (!frac_val) *ptr++= '.';
			resume_ptr = ptr;
			while (precision--) *ptr++ = '0';
		}
		
		::buffer v(buffer, ptr - buffer);
		return write(fmt, v);
	}
}

static void writed(struct fmt* fmt, double value)
{
	/*return error::unsupported;*/
	writef(fmt, (float)value);
}

