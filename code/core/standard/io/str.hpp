
void str(struct fmt* fmt, s64 value) { writes(fmt, value); }
void str(struct fmt* fmt, u64 value) { writeu(fmt, value); }
void str(struct fmt* fmt, int value) { writes(fmt, (s64)value); }
void str(struct fmt* fmt, uint value) { writeu(fmt, (u64)value); }
void str(struct fmt* fmt, short value) { writes(fmt, (s64)value); }
void str(struct fmt* fmt, ushort value) { writeu(fmt, (u64)value); }
void str(struct fmt* fmt, sbyte value) { writes(fmt, (s64)value); }
void str(struct fmt* fmt, byte value) { writeu(fmt, (u64)value); }
void str(struct fmt* fmt, bool value) { writeu(fmt, (u64)value); }
void str(struct fmt* fmt, float value) { writef(fmt, value); }
void str(struct fmt* fmt, double value) { writed(fmt, value); }
void str(struct fmt* fmt, char c) { writec(fmt, c); }
void str(struct fmt* fmt, const char* c) { writea(fmt, c); }
void str(struct fmt* fmt, buffer arg) { write(fmt, arg); }
void str(struct fmt* fmt, memory::stats value)
{
	writea(fmt, "Blocks: ");
	writeu(fmt, value.blocks);
	writea(fmt, "\nUsed: ");
	writeu(fmt, value.used);
	writea(fmt, "\nFragmented: ");
	writeu(fmt, value.fragmented);
	writea(fmt, "\nAvailable: ");
	writeu(fmt, value.available);
	writea(fmt, ".");
}
