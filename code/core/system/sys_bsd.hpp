void crash_handler(int sig)
{
	constexpr int capacity = 64;
	void* arr[capacity];
	var size  = backtrace(arr, capacity);
	var trace = backtrace_symbols(arr, size);

	//print("Crash handler (%v):", sig);
	if (trace)
	{
		for (var i = 0; i < size; i++)
		{
			var entry = trace[i];
			if (!entry) break;
			//print(">>>> %v", entry);
		}
	}
	else
	{
		//print(">>>> No trace found.");
	}
	
	exit(1);
}


error init(struct os* system, int argc, char* argv[])
{
	IF_DEBUG({
		signal(10, crash_handler); // SIGBUS  bus error
		signal(11, crash_handler); // SIGSEGV segmentation violation
	});
	
	clear(system, sizeof(struct os));
	int pagesize = getpagesize();

	// Info
	{
		system->name = "BSD";
		system->arguments.length = argc;
		system->arguments.data = argv;
	}
	
	// Memory
	{
		var sentinel = &system->allocator_sentinel;
		sentinel->_previous = sentinel;
		sentinel->_next = sentinel;

		memory::allocator allocator {};
		allocator.pagesize = pagesize;
		allocator.min = 1 * MEGABYTE;
		allocator.current = sentinel;
		system->allocator = allocator;
	}
	
	// Shared data
	{
		constexpr flags32 flags = memory::SHARED;
		var shared_block = allocate(pagesize, sizeof(os::shared), flags);
		var shared = (struct os::shared*)shared_block.data;

		var sentinel = &shared->allocator_sentinel;
		sentinel->_previous = sentinel;
		sentinel->_next = sentinel;

		memory::allocator allocator {};
		allocator.pagesize = pagesize;
		allocator.min = 1 * MEGABYTE;
		allocator.current = sentinel;
		allocator.flags = memory::SHARED;
		shared->allocator = allocator;

		system->shared = shared;
	}

	// Other
	{
		system->time = system_time();
	}
	
	return error::ok;
}
