struct os
{
	struct shared
	{
		// Data accessible to all created threads and processes
		struct memory::allocator allocator;
		struct memory::managed allocator_sentinel;
	}* shared;

	string_slice name;
	slice<char*> arguments; // make this fancy: a dictionary with an any type
	struct memory::allocator allocator;
	struct memory::managed allocator_sentinel;
	system_time time;
};

memory::pool pool(struct os* os, uptr alignment)
{
	memory::pool result {};
	result.allocator = &os->allocator;
	result.alignment = alignment;
	return result;
}

memory::pool pool(struct os::shared* os, uptr alignment)
{
	memory::pool result {};
	result.allocator = &os->allocator;
	result.alignment = alignment;
	return result;
}
