error init(struct os* system, int argc, char* argv[])
{
	clear(system, sizeof(struct os));
	int pagesize = 65536;
	
	// Info
	{
		system->name = "WASM";
		system->arguments.length = argc;
		system->arguments.data = argv;
	}
	
	// Memory
	{
		var sentinel = &system->allocator_sentinel;
		sentinel->_previous = sentinel;
		sentinel->_next = sentinel;
		
		memory::allocator allocator {};
		allocator.pagesize = pagesize;
		allocator.min = 1 * MEGABYTE;
		allocator.current = sentinel;
		system->allocator = allocator;
	}
	
	// Shared data
	{
		constexpr flags32 flags = memory::SHARED;
		var shared_block = allocate(pagesize, sizeof(os::shared), flags);
		var shared = (struct os::shared*)shared_block.data;
		
		var sentinel = &shared->allocator_sentinel;
		sentinel->_previous = sentinel;
		sentinel->_next = sentinel;
		
		memory::allocator allocator {};
		allocator.pagesize = pagesize;
		allocator.min = 1 * MEGABYTE;
		allocator.current = sentinel;
		allocator.flags = memory::SHARED;
		shared->allocator = allocator;
		
		system->shared = shared;
	}
	
	// Other
	{
		system_time st = system_time();
		init(&st);
		system->time = st;
	}
	
	return error::ok;
}
