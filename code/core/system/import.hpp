#include "os.hpp"

#if PLATFORM_WIN
	#include "sys_win.hpp"
#elif PLATFORM_WASM
	#include "sys_wasm.hpp"
#elif PLATFORM_APPLE
	#include "sys_apple.hpp"
#elif PLATFORM_ANDROID
	//#include "sys_unix.hpp"
	#include "sys_android.hpp"
#elif PLATFORM_BSD
	//#include "sys_unix.hpp"
	#include "sys_bsd.hpp"
#elif PLATFORM_LINUX
	//#include "sys_unix.hpp"
	#include "sys_linux.hpp"
#else
	#error Unknown platform
#endif
