#if NEARNE64
	#define null nullptr
	typedef      long unsigned int uptr;
	typedef      long   signed int sptr;
	typedef      long unsigned int flags64;
	typedef      long unsigned int u64;
	typedef      long   signed int s64;
#elif NEARNE32
	#define null 0
	typedef           unsigned int uptr;
	typedef             signed int sptr;
	typedef long long unsigned int flags64;
	typedef long long unsigned int u64;
	typedef long long   signed int s64;
#else
	#error Unsupported architecture!
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;


// Special datatypes
typedef u16 wasm_error;
typedef void* libhandle;
typedef long platform_long; 
typedef unsigned long platform_ulong;
typedef long long platform_llong;
typedef unsigned long long platform_ullong;
#define long #error use platform_long instead
#define ulong #error use platform_ulong instead




// EXTERNAL foreigns
#define foreign extern "C"
#define mexport extern "C" __attribute__((used))
#define mforeign(NAME) __attribute__((import_module("env"), import_name(#NAME)))


foreign void* stdout;
foreign uptr fwrite(const void*, uptr, uptr, void*);
foreign int fflush(void*);

/**
 * emsripten
 * 
 */
typedef void (*em_callback_func)(void);
typedef void (*em_arg_callback_func)(void*);
typedef void (*em_str_callback_func)(const char *);
foreign void emscripten_console_log(const char*);
foreign void emscripten_console_warn(const char*);
foreign void emscripten_console_error(const char*);
foreign void emscripten_run_script(const char*);
foreign int emscripten_run_script_int(const char*);
foreign char *emscripten_run_script_string(const char*);
foreign void emscripten_set_main_loop(em_callback_func func, int fps, int simulate_infinite_loop);
foreign char *emscripten_get_window_title();
foreign void emscripten_set_window_title(const char*);
foreign void emscripten_get_screen_size(int *width, int *height);
foreign void emscripten_hide_mouse(void);
foreign double emscripten_get_now(void);
foreign float emscripten_random(void);

/** wasi
 * - The time since the start of the program
 */
foreign wasm_error __wasi_clock_res_get(u32, u64*) __attribute__((
    __import_module__("wasi_snapshot_preview1"),
    __import_name__("clock_res_get")
));
foreign wasm_error __wasi_clock_time_get(u32, u64, u64*) __attribute__((
    __import_module__("wasi_snapshot_preview1"),
    __import_name__("clock_time_get")
));


/** usr/include/sys/mman.h
 * - Allocate and free large blocks of memory
 */
foreign void* mmap(void*, uptr, int, int, int, s64);
foreign int munmap(void*, uptr);
foreign int mprotect(void*, uptr, int);


/**
 * math.h
 * - specific non-libc externals
 */
#define __pure2 __attribute__((__const__))
foreign int isinf(double);
foreign int isnan(double);
foreign int ilogbf(float) __pure2;
foreign float fabsf(float) __pure2;
foreign float copysignf(float, float) __pure2;
foreign float nanf(const char *) __pure2;
foreign float fmaxf(float, float) __pure2;
foreign float fminf(float, float) __pure2;
#define abs fabsf


/**
 * math funcitons the platform has not implmented
 */
inline bool isnan(float x) { return x != x; }
