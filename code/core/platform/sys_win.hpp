#define _CRT_SECURE_NO_WARNINGS
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#define WIN_ONLY(x) x
#define foreign extern "C"
#define NEARNE_WIN 1
#if NEARNE64
	#define _AMD64_
	#define null nullptr
	typedef long long unsigned int uptr;
	typedef long long   signed int sptr;
	typedef long long unsigned int flags64;
	typedef long long unsigned int u64;
	typedef long long   signed int s64;
#else
	#define _X86_
	#define null 0
	typedef           unsigned int uptr;
	typedef             signed int sptr;
	typedef long long unsigned int flags64;
	typedef long long unsigned int u64;
	typedef long long   signed int s64;
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;

typedef HMODULE       libhandle;
typedef long          platform_long;
typedef unsigned long platform_ulong;
typedef long long platform_llong;
typedef unsigned long long platform_ullong;
#define long USE_PLATFORM_LONG_INSTEAD


/** stdio.h
 * 
 */
foreign void* __cdecl __acrt_iob_func(unsigned _Ix);
#define stdout (__acrt_iob_func(1))


/**
 * math.h
 * - specific non-libc externals
 */
foreign float fabsf(float);
#define abs   fabsf

foreign short __cdecl _dtest(_In_ double* _Px);
foreign short __cdecl _ldtest(_In_ long double* _Px);
foreign short __cdecl _fdtest(_In_ float* _Px);
inline bool isinf(float x) { return _fdtest(&x) == 1; }
inline bool isinf(double x) { return _dtest(&x) == 1; }
inline bool isinf(long double x) { return _ldtest(&x) == 1; }
inline bool isnan(float x) { return _fdtest(&x) == 2; }
inline bool isnan(double x) { return _dtest(&x) == 2; }
inline bool isnan(long double x) { return _ldtest(&x) == 2; }

inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }
