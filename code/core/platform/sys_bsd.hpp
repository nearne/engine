/** usr/include/stdio.h
 * - Printing to console
 */
foreign void* __stdoutp;
#define stdout __stdoutp


/**
 * math.h
 * - specific non-libc externals
 */
foreign double drem(double, double);
foreign int	   finite(double) __pure2;
foreign int	   isnanf(float) __pure2;
foreign double gamma_r(double, int *);
foreign double lgamma_r(double, int *);
foreign double significand(double);
#define isnan  isnanf


/** usr/include/unistd.h
 *
 */
foreign platform_long sysconf(int);
foreign int rfork(int) __attribute__((__returns_twice__));
foreign int rfork_thread(int, void*, int(*)(void*), void*);



inline int getpagesize() { return sysconf(47); } // _SC_PAGESIZE
