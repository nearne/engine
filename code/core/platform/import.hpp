/**
 * == PLATFORM ==
 * The platform section is focussed on the operating system. It defines the
 * fundamental types and links to operating system routines.
 * 
 * TODO: find and provide procedures to a non-blocking I/O solution.
 */

#if _WIN64 || _WIN32
	#define PLATFORM_WIN 1
	#include "sys_win.hpp"
	#include "libc.hpp"
#elif __wasm__
	#define PLATFORM_WASM 1
	#include "sys_wasm.hpp"
	#include "libc.hpp"
#elif __APPLE__
	#define PLATFORM_APPLE 1
	#include "sys_apple.hpp"
	#include "libc.hpp"
#elif __ANDROID__
	#define PLATFORM_ANDROID 1
	#include "sys_unix.hpp"
	#include "sys_android.hpp"
	#include "libc.hpp"
#elif __FreeBSD__
	#define PLATFORM_BSD 1
	#include "sys_unix.hpp"
	#include "sys_bsd.hpp"
	#include "libc.hpp"
#elif __linux__
	#define PLATFORM_LINUX 1
	#include "sys_unix.hpp"
	#include "sys_linux.hpp"
	#include "libc.hpp"
#else
	#error Unknown platform
#endif

static_assert(sizeof(int) == 4, "Size of an integer should be 32 bits.");
static_assert(sizeof(u64) == 8, "Size of a u64 should be 64 bits.");
