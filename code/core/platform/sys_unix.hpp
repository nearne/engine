#define PLATFORM_UNIX 1
#if NEARNE64
	#define null nullptr
	typedef      long unsigned int uptr;
	typedef      long   signed int sptr;
	typedef      long unsigned int flags64;
	typedef      long unsigned int u64;
	typedef      long   signed int s64;
#elif NEARNE32
	#define null 0
	typedef           unsigned int uptr;
	typedef             signed int sptr;
	typedef long long unsigned int flags64;
	typedef long long unsigned int u64;
	typedef long long   signed int s64;
#else
	#error Unsupported architecture!
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;


// Special datatypes
typedef void* libhandle;
typedef long platform_long;
typedef unsigned long platform_ulong;
typedef long long platform_llong;
typedef unsigned long long platform_ullong;
#define long #error use platform_long instead
#define ulong #error use platform_ulong instead



// EXTERNAL foreigns
// https://man7.org/linux/man-pages
// https://linux.die.net/man
#define foreign extern "C"


/** usr/include/time.h
 * - The time since the start of the program
 */
foreign int clock_getres(int, void*);
foreign int clock_gettime(int, void*);

/** usr/include/sys/mman.h
 * - Allocate and free large blocks of memory
 */
foreign void* mmap(void*, uptr, int, int, int, sptr);
foreign int munmap(void*, uptr);
foreign int mprotect(void*, uptr, int);

/** us/include/dlfcn.h
 * - Open and close dynamically loaded libraries
 */
foreign int dlclose(void*);
foreign char* dlerror();
foreign libhandle dlopen(const char*, int);
foreign void* dlsym(libhandle, const char*);


/**
 * math.h
 * - specific non-libc externals
 */
#define __pure2 __attribute__((__const__))
foreign int isinf(double);
foreign int isnan(double);
foreign int ilogbf(float) __pure2;
foreign float fabsf(float) __pure2;
foreign float copysignf(float, float) __pure2;
foreign float nanf(const char *) __pure2;
foreign float fmaxf(float, float) __pure2;
foreign float fminf(float, float) __pure2;
#define abs fabsf

/** usr/include/sys/signal.h
 * - register procedure pointer to application signal (such as segmentation violation).
 */
typedef void (*proc_sighandler)(int);
foreign proc_sighandler signal(int, proc_sighandler);


/** usr/include/execinfo.h
 * - retrieve stacktrace.
 */
foreign int backtrace(void**, int);
foreign char** backtrace_symbols(void*const*, int);


/** ? probably stdlib??
 * - the address of the errno variable for the current thread.
 */
foreign int* __errno_location (void);

/** usr/include/sys/sysctl.h
 *
 */
foreign int sysctl(const int*, uint, void*, uptr*, const void*, u64);


/** usr/include/unistd.h
 * - Reading and writing of devices such as joysticks
 * - Uses the handle retrieved in the procedure of fcntl.h::open
 */
foreign sptr read(int, void*, uptr);
foreign sptr write(int, const void*, uptr);
foreign int  close(int);
foreign void _exit(int);
foreign s64 readlink(const char*, void*, u64);

/** usr/include/sys/uio.h
 * - Reading and writing asynchronosouly
 * 
 * 
 */
foreign sptr readv(int, const void*, int);
foreign sptr writev(int, const void*, int);
