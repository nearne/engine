/**
 * math.h
 * - specific non-libc externals
 */
inline int isnan(float x) { return x != x; }
foreign float fabsf(float) __pure2;

/** usr/include/unistd.h
 *
 */
foreign int vfork(void);// OBSOLETE
foreign int getpagesize(void);


/**
 * https://linux.die.net/man/2/clone
 */

// https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/sched.h#L19
constexpr int clone_vm = 0x00000100;
constexpr int clone_fs =	0x00000200;	/* set if fs info shared between processes */
constexpr int clone_files =	0x00000400;	/* set if open files shared between processes */
constexpr int clone_sighand =	0x00000800;	/* set if signal handlers and blocked signals shared */
constexpr int clone_pidfd =	0x00001000;	/* set if a pidfd should be placed in parent */
constexpr int clone_ptrace =	0x00002000;	/* set if we want to let tracing continue on the child too */
constexpr int clone_vfork =	0x00004000;	/* set if the parent wants the child to wake it up on mm_release */
constexpr int clone_parent =	0x00008000;	/* set if we want to have the same parent as the cloner */
constexpr int clone_thread =	0x00010000;	/* Same thread group? */
constexpr int clone_newns =	0x00020000;	/* New mount namespace group */
constexpr int clone_sysvsem =	0x00040000;	/* share system V SEM_UNDO semantics */
constexpr int clone_settls =	0x00080000;	/* create a new TLS for the child */
constexpr int clone_parent_settid =	0x00100000;	/* set the TID in the parent */
constexpr int clone_child_cleartid =	0x00200000;	/* clear the TID in the child */
constexpr int clone_detached =		0x00400000;	/* Unused, ignored */
constexpr int clone_untraced =		0x00800000;	/* set if the tracing process can't force CLONE_PTRACE on this clone */
constexpr int clone_child_settid =	0x01000000;	/* set the TID in the child */
constexpr int clone_newcgroup =		0x02000000;	/* New cgroup namespace */
constexpr int clone_newuts =		0x04000000;	/* New utsname namespace */
constexpr int clone_newipc =		0x08000000;	/* New ipc namespace */
constexpr int clone_newuser =		0x10000000;	/* New user namespace */
constexpr int clone_newpid =		0x20000000;	/* New pid namespace */
constexpr int clone_newnet =		0x40000000;	/* New network namespace */
constexpr int clone_io =		0x80000000;	/* Clone io context */
foreign int clone(int(*fn)(void*), void*, int, void*);
