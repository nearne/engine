#include <jni.h>

/**
 * math.h
 * - specific non-libc externals
 */
inline int isnan(float x) { return x != x; }
foreign float fabsf(float) __pure2;

/** usr/include/unistd.h
 *
 */
foreign int vfork(void);
