/**
 * Common types
 * size_t   : uptr
 * ssize_t  : sptr
 * off_t    : sptr
 * offset_t : s64
 */

/** usr/include/stdlib.h
 * @Obsolete malloc use mmap instead
 * - Allocating memory on the heap
 * - Exit the program from anywhere
 */
foreign void* malloc(uptr);
foreign void exit(int);

/** usr/include/stdio.h
 * TODO: Try removing some included procedures
 * - Printing to console
 * - Reading and writing of files
 */
foreign void* fopen(const char*, const char*);
foreign int fclose(void*);
foreign int fflush(void*);
foreign uptr fread(void*, uptr, uptr, void*);
foreign uptr fwrite(const void*, uptr, uptr, void*);
foreign int fseek(void*, platform_long, int);
foreign platform_long ftell(void*);


/** usr/include/sys/ioccom.h
 * - Reading and writing of devices such as joysticks
 * - fcntl.h open
 */
foreign int ioctl(int, platform_ulong, ...);


/** usr/include/fcntl.h
 * - Opening devices such as audio devices and joysticks
 */
foreign int open(const char*, int, ...);


/** usr/include/unistd.h
 * - Reading and writing of devices such as joysticks
 * - Uses the handle retrieved in the procedure of fcntl.h::open
 */
foreign sptr read(int, void*, uptr);


/** usr/include/sys/ioccom.h
 * - Reading and writing of devices such as joysticks
 * - fcntl.h open
 */
foreign int ioctl(int, platform_ulong, ...);


/** math.h
 * 
 */
foreign float acosf(float);
foreign float asinf(float);
foreign float atanf(float);
foreign float atan2f(float, float);
foreign float cosf(float);
foreign float sinf(float);
foreign float tanf(float);
foreign float coshf(float);
foreign float sinhf(float);
foreign float tanhf(float);
foreign float exp2f(float);
foreign float expf(float);
foreign float expm1f(float);
foreign float frexpf(float, int *);	/* fundamentally !__pure2 */
foreign float ldexpf(float, int);
foreign float log10f(float);
foreign float log1pf(float);
foreign float log2f(float);
foreign float logf(float);
foreign float modff(float, float *);	/* fundamentally !__pure2 */
foreign float powf(float, float);
foreign float sqrtf(float);
foreign float ceilf(float);
foreign float floorf(float);
foreign float fmodf(float, float);
foreign float roundf(float);
foreign float erff(float);
foreign float erfcf(float);
foreign float hypotf(float, float);
foreign float lgammaf(float);
foreign float tgammaf(float);
foreign float acoshf(float);
foreign float asinhf(float);
foreign float atanhf(float);
foreign float cbrtf(float);
foreign float logbf(float);
foreign platform_llong llrintf(float);
foreign platform_llong llroundf(float);
foreign platform_long lrintf(float);
foreign platform_long lroundf(float);
foreign float nearbyintf(float);
foreign float nextafterf(float, float);
foreign float remainderf(float, float);
foreign float remquof(float, float, int *);
foreign float rintf(float);
foreign float scalblnf(float, platform_long);
foreign float scalbnf(float, int);
foreign float truncf(float);
foreign float fdimf(float, float);
foreign float fmaf(float, float, float);

#define acos  acosf
#define asin  asinf
#define atan  atanf
#define atan2 atan2f
#define ceil  ceilf
#define cbrt  cbrtf
#define cos   cosf
#define cosh  coshf
#define exp   expf
#define exp2  exp2f
#define floor floorf
#define fmod  fmodf
#define log   logf
#define log10 log10f
#define log2  log2f
#define modf  modff
#define pow   powf
#define round roundf
#define sin   sinf
#define sinh  sinhf
#define sqrt  sqrtf
#define tan   tanf
#define tanh  tanhf
#define trunc truncf
