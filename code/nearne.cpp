#define NEARNE_APP_NAME "Test Application"
#define NEARNE_DEBUG 1

#include "core/import.hpp"
#include "driver/import.hpp"

struct nearne
{
	struct os os;
};

int main(int argc, char* argv[])
{
	IF_DEBUG({
		test_core();
	});
	
	var nearne = new struct nearne();
	init(&nearne->os, argc, argv);
	
	print("Testing memory pool");
	var result = pool(&nearne->os, 64);
	{
		var test = pool(nearne->os.shared, 64);
		var shared_data = request(&test, 4096);
		var stack = request(&test, 4096);
		var stack_end = stack.data + stack.length;
		print("First element at array: %v", (int)shared_data[0]);
		print("Stack end: %v", stack_end);
	}
	
	var current = result.current;
	while (current)
	{
		result.current->data[result.current->length] = 0;
		current = current->previous;
	}
	
	print("\n\n-------------------");
	print("Memory allocator stats:");
	print(stats(nearne->os.allocator));
	print("\n\n");
	return 0;
}
